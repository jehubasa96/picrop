package com.example.jayze.picrop;

import android.app.Dialog;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

/**
 * Created by jayze on 09/03/2016.
 */
public class MainActivity extends AppCompatActivity implements HomeFragment.OnGetImageListner,FiltersFragment.OnGetImageListner{
    Toolbar toolbar;
    TabLayout tabLayout;
    ViewPager viewPager;
    ViewPagerAdapter viewPagerAdapter;
    ImageView finish;
    Button save,cancel;
    EditText fileName;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        toolbar=(Toolbar)findViewById(R.id.toolbar);
        toolbar.setTitle("PiCrop Mobile");
        setSupportActionBar(toolbar);
        finish=(ImageView)findViewById(R.id.imageStorage);
        tabLayout=(TabLayout)findViewById(R.id.tabLayout);
        viewPager=(ViewPager)findViewById(R.id.viewPager);
        viewPagerAdapter=new ViewPagerAdapter(getSupportFragmentManager());
        viewPagerAdapter.addFragments(new HomeFragment(),"HOME");
        viewPagerAdapter.addFragments(new FiltersFragment(),"FILTERS");
        viewPagerAdapter.addFragments(new AboutFragment(),"ABOUT");
        viewPager.setAdapter(viewPagerAdapter);
        tabLayout.setupWithViewPager(viewPager);
    }

    @Override
    public void setImage(ImageView img) {
        FiltersFragment f2= (FiltersFragment) viewPagerAdapter.getItem(1);
        f2.updateImage(img);
    }

    @Override
    public void setFinishImage(ImageView img) {
        finish.setImageDrawable(img.getDrawable());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater=getMenuInflater();
        menuInflater.inflate(R.menu.main_menu,menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if(item.getItemId()==R.id.saveFile){
            final Dialog dialog=new Dialog(MainActivity.this);
            dialog.setContentView(R.layout.save_file);
            Window window=dialog.getWindow();
            window.setLayout(WindowManager.LayoutParams.MATCH_PARENT,WindowManager.LayoutParams.WRAP_CONTENT);
            dialog.show();

            save=(Button)dialog.findViewById(R.id.saveButton);
            cancel=(Button)dialog.findViewById(R.id.cancelButton);
            fileName=(EditText)dialog.findViewById(R.id.fileNameText);
            save.setOnClickListener(new View.OnClickListener() {

                        @Override
                        public void onClick(View v) {
                            OutputStream output;
                            Bitmap bitmap=((BitmapDrawable)finish.getDrawable()).getBitmap();
                            File dir=new File("sdcard/PiCrop");
                                    if(dir.exists()){
                                        dir.mkdir();
                                    }
                            File save = new File(dir, fileName.getText().toString()+".jpg");
                            try {
                                output = new FileOutputStream(save);
                                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, output);
                                output.flush();
                                output.close();
                                Toast.makeText(MainActivity.this, "File Saved", Toast.LENGTH_SHORT).show();
                            } catch (FileNotFoundException e) {
                                e.printStackTrace();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }

                            dialog.dismiss();
                          }

                         });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
            }
        });
    }
        return true;
}


}
