package com.example.jayze.picrop;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;


/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment {

    private ImageView imageView;
    ImageButton camera,file;
    static final int cam_req=1;
    static final int image_req=2;
    OnGetImageListner onGetImageListner;

    public HomeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView=inflater.inflate(R.layout.fragment_home, container, false);
        // Inflate the layout for this fragment

        //initialize camera option
        camera=(ImageButton)rootView.findViewById(R.id.cameraButton);

        //initializer ImaveView
        imageView=(ImageView)rootView.findViewById(R.id.image);

        //set OnClickListner for camera button
        camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent cameraIntent=new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                File file=getFile();
                cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(file));
                startActivityForResult(cameraIntent, cam_req);

            }
        });

        //initialize existing file option
        file=(ImageButton)rootView.findViewById(R.id.fileButton);

        //if you click the file option
        file.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent getImageToSD = new Intent(Intent.ACTION_PICK);
                //get the picture to:
                String picturePath = (Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES)).getPath();

                Uri data = Uri.parse(picturePath);

                getImageToSD.setDataAndType(data, "image/*");

                startActivityForResult(getImageToSD, image_req);


            }
        });

        //if you click the image in home the image will reset
        if(imageView!=null){
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onGetImageListner.setImage(imageView);
            }
        });}
        return rootView;
    }

    private File getFile(){
        //create folder for Picrop Image
        File file=new File("sdcard/PiCrop");

        //checking if folder or destination is already existed
        if(file.exists()){
            file.mkdir();
        }

        //file naming
        File image_file =new File(file,"PiCrop_IMG.jpg");
        return image_file;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==1){
        String path="sdcard/PiCrop/PiCrop_IMG.jpg";
        imageView.setImageDrawable(Drawable.createFromPath(path));
            if(imageView==null){
                Toast.makeText(getActivity(),"bitmap is null",Toast.LENGTH_SHORT).show();
            }

       }
        else if(requestCode==2){
            //get the address of the image
            Uri imageAddress=data.getData();

            //create stream to read the image data from the sdCard
            InputStream inputStream;

            //get inputStream base on URI
            try {
                inputStream=getActivity().getContentResolver().openInputStream(imageAddress);

                //get bitmap from inputstream
                Bitmap image=BitmapFactory.decodeStream(inputStream);

                //set image to ImageView
                imageView.setImageBitmap(image);

                if(image==null){
                    Toast.makeText(getActivity(),"bitmap is null",Toast.LENGTH_SHORT).show();
                }


            } catch (FileNotFoundException e) {
                e.printStackTrace();

                Toast.makeText(getActivity(),"PiCrop cannot read your file",Toast.LENGTH_SHORT).show();
            }
        }

        onGetImageListner.setImage(imageView);

    }


    public interface OnGetImageListner{
        void setImage(ImageView img);
    }

    @SuppressWarnings("deprecation")
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            onGetImageListner = (OnGetImageListner) activity;
        }catch (Exception e){

        }
    }


}
