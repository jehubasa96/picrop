package com.example.jayze.picrop;


import android.app.Activity;
import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Paint;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.renderscript.Allocation;
import android.renderscript.Element;
import android.renderscript.RenderScript;
import android.renderscript.ScriptIntrinsicBlur;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.Toast;

/**
 * A simple {@link Fragment} subclass.
 */
public class FiltersFragment extends Fragment {
    OnGetImageListner onGetImageListner;
    EditText factorText,offsetText,gR,gG,gB;
    LinearLayout setEmboss,setGamma;
    Button embossAccept,gammaAccept;
    SeekBar seekbar,brSeekBar,blurSeekBar;
    ImageView filterimg;
    ImageButton bw,contrast,brightness,blur,emboss,invert,gamma;
    int contrastValue,brightnessValue;

    GreyscaleAsyncTask greyscaleAsyncTask;
    BrightContrastAsyncTask brightContrastAsyncTask;
    BlurAsyncTask blurAsyncTask;
    EmbossAsyncTask embossAsyncTask;
    InvertAsyncTask invertAsyncTask;
    GammaAsyncTask gammaAsyncTask;

    public FiltersFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView=inflater.inflate(R.layout.fragment_filters, container, false);
        filterimg=(ImageView)rootView.findViewById(R.id.filterImgView);

        seekbar=(SeekBar)rootView.findViewById(R.id.seekBar);
        brSeekBar=(SeekBar)rootView.findViewById(R.id.brightseekBar);
        blurSeekBar=(SeekBar)rootView.findViewById(R.id.blurseekBar);
        factorText=(EditText)rootView.findViewById(R.id.embossFactor);
        offsetText=(EditText)rootView.findViewById(R.id.embossOffset);
        embossAccept=(Button)rootView.findViewById(R.id.embossAccept);
        setEmboss=(LinearLayout)rootView.findViewById(R.id.setEmboss);
        gR=(EditText)rootView.findViewById(R.id.redText);
        gG=(EditText)rootView.findViewById(R.id.greenText);
        gB=(EditText)rootView.findViewById(R.id.blueText);
        gammaAccept=(Button)rootView.findViewById(R.id.gammaAccept);
        setGamma=(LinearLayout)rootView.findViewById(R.id.setGamma);

        if(contrastValue==0){
            contrastValue=1;
        }

        //greyScale
        bw=(ImageButton)rootView.findViewById(R.id.blackAndWhiteButton);
        bw.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (filterimg!=null){
                    Bitmap graymap=((BitmapDrawable)filterimg.getDrawable()).getBitmap();
                    greyscaleAsyncTask=new GreyscaleAsyncTask(graymap);
                    greyscaleAsyncTask.execute();
                    graymap=null;
                }

            }
        });

        //Contrast
        contrast=(ImageButton)rootView.findViewById(R.id.contrastButton);
        contrast.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                seekbar.setVisibility(View.VISIBLE);
                seekbar.setProgress(contrastValue);
                seekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                    int cProgress;
                    @Override
                    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

                        cProgress=progress;
                    }

                    @Override
                    public void onStartTrackingTouch(SeekBar seekBar) {

                    }

                    @Override
                    public void onStopTrackingTouch(SeekBar seekBar) {
                        if(filterimg!=null){
                            Bitmap bitmap=((BitmapDrawable)filterimg.getDrawable()).getBitmap();
                            contrastValue=cProgress;
                            brightContrastAsyncTask=new BrightContrastAsyncTask(bitmap,cProgress,brightnessValue);
                            brightContrastAsyncTask.execute();
                            bitmap = null;
                        }
                            seekBar.setVisibility(View.GONE);
                    }
                });
            }
        });

        //brightness
        brightness=(ImageButton)rootView.findViewById(R.id.brightnessButton);
        brightness.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                brSeekBar.setVisibility(View.VISIBLE);
                brSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                    @Override
                    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

                        if(progress<256){
                            int brprogress=progress-256;
                            if(brprogress==-256){
                                brprogress=-255;
                            }
                            brightnessValue=brprogress;
                        }
                        else if(progress==256){
                            int brprogress=0;
                            brightnessValue=brprogress;
                        }
                        else if (progress>256){
                            int brprogress=(progress-256);
                            if(brprogress==0){
                                brprogress=1;
                            }
                            brightnessValue=brprogress;
                        }
                    }

                    @Override
                    public void onStartTrackingTouch(SeekBar seekBar) {
                    }

                    @Override
                    public void onStopTrackingTouch(SeekBar seekBar) {
                        if(filterimg!=null){
                            Bitmap bitmap=((BitmapDrawable)filterimg.getDrawable()).getBitmap();
                            brightContrastAsyncTask=new BrightContrastAsyncTask(bitmap, contrastValue, brightnessValue);
                            brightContrastAsyncTask.execute();
                            bitmap = null;
                        }
                        brSeekBar.setVisibility(View.GONE);
                    }
                });
            }
        });

        //blur
        blur=(ImageButton)rootView.findViewById(R.id.blurButton);
        blur.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    blurSeekBar.setVisibility(View.VISIBLE);
                    blurSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                        int blurprogress;

                        @Override
                        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                            blurprogress = progress;
                        }

                        @Override
                        public void onStartTrackingTouch(SeekBar seekBar) {

                        }

                        @Override
                        public void onStopTrackingTouch(SeekBar seekBar) {
                            if (filterimg != null) {
                                Drawable d=filterimg.getDrawable();
                                Bitmap blurmap=Bitmap.createBitmap(d.getIntrinsicWidth(), d.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
                                Canvas canvas = new Canvas(blurmap);
                                d.draw(canvas);
                                blurAsyncTask=new BlurAsyncTask(blurmap, blurprogress);
                                blurAsyncTask.execute();
                                blurmap = null;

                            }

                            blurSeekBar.setVisibility(View.GONE);
                        }
                    });
                }

            });
        //emboss
        emboss=(ImageButton)rootView.findViewById(R.id.embossButton);
        emboss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setEmboss.setVisibility(View.VISIBLE);
                embossAccept.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (filterimg != null) {

                            double factor=Double.parseDouble(factorText.getText().toString());
                            double offset=Double.parseDouble(offsetText.getText().toString());
                            if(factor>100){
                                factor=100;
                            }
                            else if (factor<0){
                                factor=0;
                            }
                            if(offset>500){
                                offset=500;
                            }
                            else if (offset<0){
                                offset=0;
                            }

                            Bitmap embossmap=((BitmapDrawable)filterimg.getDrawable()).getBitmap();
                            embossAsyncTask=new EmbossAsyncTask(embossmap,factor,offset);
                            embossAsyncTask.execute();
                            embossmap = null;
                            setEmboss.setVisibility(View.GONE);
                        }
                    }
                });
            }
        });

        //INvert
        invert=(ImageButton)rootView.findViewById(R.id.invertButton);
        invert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    if (filterimg != null) {
                        Bitmap invertMap=((BitmapDrawable)filterimg.getDrawable()).getBitmap();
                        invertAsyncTask=new InvertAsyncTask(invertMap);
                        invertAsyncTask.execute();
                        invertMap = null;
                    }
            }
        });

        //gamma
        gamma=(ImageButton)rootView.findViewById(R.id.gammaButton);
        gamma.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setGamma.setVisibility(View.VISIBLE);

                gammaAccept.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (filterimg != null) {

                            double red = Double.parseDouble(gR.getText().toString());
                            double green = Double.parseDouble(gG.getText().toString());
                            double blue = Double.parseDouble(gB.getText().toString());
                            if (red > 100) {
                                red = 100;
                            } else if (red < 0.9) {
                                red = 0.9;
                            }
                            if (green > 100) {
                                green = 100;
                            } else if (green < 0.9) {
                                green = 0.9;
                            }
                            if (blue > 100) {
                                blue = 100;
                            } else if (blue < 0.9) {
                                blue = 0.9;
                            }

                            Bitmap gammamap=((BitmapDrawable)filterimg.getDrawable()).getBitmap();
                            gammaAsyncTask=new GammaAsyncTask(gammamap, red, green, blue);
                            gammaAsyncTask.execute();
                            gammamap = null;
                            setGamma.setVisibility(View.GONE);
                        }
                    }
                });
            }
        });


        // Inflate the layout for this fragment
        return rootView;
    }

    public void updateImage(ImageView img){
        seekbar.setProgress(1);
        brSeekBar.setProgress(256);
        blurSeekBar.setProgress(1);
        factorText.setText("1");
        offsetText.setText("127");
        gR.setText("1.8");
        gG.setText("1.8");
        gB.setText("1.8");
        filterimg.setImageDrawable(img.getDrawable());
    }


    class BrightContrastAsyncTask extends AsyncTask<Void, Integer, Void>{
        Bitmap bmp,result;
        float contrast,brightness;
        ProgressDialog progressDialog;
        public BrightContrastAsyncTask(Bitmap bmp, float contrast, float brightness){
            this.bmp=bmp;
            this.contrast=contrast;
            this.brightness=brightness;
        }

        @Override
        protected Void doInBackground(Void... params) {
                ColorMatrix cm = new ColorMatrix(new float[]
                        {
                                contrast, 0, 0, 0, brightness,
                                0, contrast, 0, 0, brightness,
                                0, 0, contrast, 0, brightness,
                                0, 0, 0, 1, 0
                        });

                Bitmap ret = Bitmap.createBitmap(bmp.getWidth(), bmp.getHeight(), bmp.getConfig());

                Canvas canvas = new Canvas(ret);

                Paint paint = new Paint();
                paint.setColorFilter(new ColorMatrixColorFilter(cm));
                canvas.drawBitmap(bmp, 0, 0, paint);
                bmp=null;
                result=ret;
                ret=null;
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog=ProgressDialog.show(getActivity(),"WORKING","Please wait for a moment");
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            progressDialog.dismiss();
            Toast.makeText(getActivity(),"DONE\n(TIP: Tap the original image in the home to undo)",Toast.LENGTH_LONG).show();
            filterimg.setImageBitmap(result);
            onGetImageListner.setFinishImage(filterimg);
        }
    }

    class GreyscaleAsyncTask extends AsyncTask<Void, Integer, Void>{
        Bitmap src,result;
        ProgressDialog progressDialog;
        public GreyscaleAsyncTask(Bitmap img){
            src=img;
        }

        @Override
        protected Void doInBackground(Void... params) {
                // constant factors
                final double GS_RED = 0.299;
                final double GS_GREEN = 0.587;
                final double GS_BLUE = 0.114;

                // create output bitmap
                Bitmap bmOut = Bitmap.createBitmap(src.getWidth(), src.getHeight(), src.getConfig());
                // pixel information
                int A, R, G, B;
                int pixel;

                // get image size
                int width = src.getWidth();
                int height = src.getHeight();

                // scan through every single pixel
                for(int x = 0; x < width; ++x) {
                    for(int y = 0; y < height; ++y) {
                        // get one pixel color
                        pixel = src.getPixel(x, y);
                        // retrieve color of all channels
                        A = Color.alpha(pixel);
                        R = Color.red(pixel);
                        G = Color.green(pixel);
                        B = Color.blue(pixel);
                        // take conversion up to one single value
                        R = G = B = (int)(GS_RED * R + GS_GREEN * G + GS_BLUE * B);
                        // set new pixel color to output bitmap
                        bmOut.setPixel(x, y, Color.argb(A, R, G, B));
                    }
                }
                result=bmOut;
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog=ProgressDialog.show(getActivity(),"WORKING","Please wait for a moment");
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            progressDialog.dismiss();
            Toast.makeText(getActivity(),"DONE\n(TIP: Tap the original image in the home to undo)",Toast.LENGTH_LONG).show();
            filterimg.setImageBitmap(result);
            onGetImageListner.setFinishImage(filterimg);
        }
    }

    class BlurAsyncTask extends AsyncTask<Void, Integer, Void>{
        Bitmap image,result;
        float value;
        ProgressDialog progressDialog;
        public BlurAsyncTask(Bitmap image, float value){
            this.image=image;
            this.value=value;
        }

        @Override
        protected Void doInBackground(Void... params) {
                if(value<=0){
                    value=1;
                }
                if (null == image) return null;

                Bitmap outputBitmap = Bitmap.createBitmap(image);
                RenderScript renderScript = RenderScript.create(getActivity());
                Allocation tmpIn = Allocation.createFromBitmap(renderScript, image);
                Allocation tmpOut = Allocation.createFromBitmap(renderScript, outputBitmap);

                //Intrinsic Gausian blur filter
                ScriptIntrinsicBlur theIntrinsic = ScriptIntrinsicBlur.create(renderScript, Element.U8_4(renderScript));
                theIntrinsic.setRadius(value);
                theIntrinsic.setInput(tmpIn);
                theIntrinsic.forEach(tmpOut);
                tmpOut.copyTo(outputBitmap);
                result=outputBitmap;
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog=ProgressDialog.show(getActivity(),"WORKING","Please wait for a moment");
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            progressDialog.dismiss();
            Toast.makeText(getActivity(),"DONE\n(TIP: Tap the original image in the home to undo)",Toast.LENGTH_LONG).show();
            filterimg.setImageBitmap(result);
            onGetImageListner.setFinishImage(filterimg);
        }
    }

    class EmbossAsyncTask extends AsyncTask<Void, Integer, Void>{
        Bitmap src,result;
        double factor,offset;
        ProgressDialog progressDialog;
        public EmbossAsyncTask(final Bitmap src, final double factor, final double offset){
            this.src=src;
            this.factor=factor;
            this.offset=offset;
        }

        @Override
        protected Void doInBackground(Void... params) {
                double[][] EmbossConfig = new double[][] {
                        { -1 ,  0, -1 },
                        {  0 ,  4,  0 },
                        { -1 ,  0, -1 }
                };
                ConvolutionMatrix convMatrix = new ConvolutionMatrix(3);
                convMatrix.applyConfig(EmbossConfig);
                convMatrix.Factor = factor;
                convMatrix.Offset = offset;
                result=ConvolutionMatrix.computeConvolution3x3(src, convMatrix);

            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog=ProgressDialog.show(getActivity(),"WORKING","Please wait for a moment");
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            progressDialog.dismiss();
            Toast.makeText(getActivity(),"DONE\n(TIP: Tap the original image in the home to undo)",Toast.LENGTH_LONG).show();
            filterimg.setImageBitmap(result);
            onGetImageListner.setFinishImage(filterimg);
        }
    }

    class InvertAsyncTask extends AsyncTask<Void, Integer, Void>{
        Bitmap src,result;
        ProgressDialog progressDialog;
        public InvertAsyncTask(Bitmap src){
            this.src=src;
        }

        @Override
        protected Void doInBackground(Void... params) {
                // create new bitmap with the same settings as source bitmap
                Bitmap bmOut = Bitmap.createBitmap(src.getWidth(), src.getHeight(), Bitmap.Config.ARGB_8888);
                // color info
                int A, R, G, B;
                int pixelColor;
                // image size
                int height = src.getHeight();
                int width = src.getWidth();

                // scan through every pixel
                for (int y = 0; y < height; y++)
                {
                    for (int x = 0; x < width; x++)
                    {
                        // get one pixel
                        pixelColor = src.getPixel(x, y);
                        // saving alpha channel
                        A = Color.alpha(pixelColor);
                        // inverting byte for each R/G/B channel
                        R = 255 - Color.red(pixelColor);
                        G = 255 - Color.green(pixelColor);
                        B = 255 - Color.blue(pixelColor);
                        // set newly-inverted pixel to output image
                        bmOut.setPixel(x, y, Color.argb(A, R, G, B));
                    }
                }
                src=null;
                // return final bitmap
                result=bmOut;

            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog=ProgressDialog.show(getActivity(),"WORKING","Please wait for a moment");
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            progressDialog.dismiss();
            Toast.makeText(getActivity(),"DONE\n(TIP: Tap the original image in the home to undo)",Toast.LENGTH_LONG).show();
            filterimg.setImageBitmap(result);
            onGetImageListner.setFinishImage(filterimg);
        }
    }

    class GammaAsyncTask extends AsyncTask<Void, Integer, Void>{
        Bitmap src,result;
        double red,green,blue;
        ProgressDialog progressDialog;
        public GammaAsyncTask(Bitmap src, double red, double green, double blue){
            this.src=src;
            this.red=red;
            this.green=green;
            this.blue=blue;
        }

        @Override
        protected Void doInBackground(Void... params) {
                // create output image
                Bitmap bmOut = Bitmap.createBitmap(src.getWidth(), src.getHeight(), src.getConfig());
                // get image size
                int width = src.getWidth();
                int height = src.getHeight();
                // color information
                int A, R, G, B;
                int pixel;
                // constant value curve
                final int    MAX_SIZE = 256;
                final double MAX_VALUE_DBL = 255.0;
                final int    MAX_VALUE_INT = 255;
                final double REVERSE = 1.0;

                // gamma arrays
                int[] gammaR = new int[MAX_SIZE];
                int[] gammaG = new int[MAX_SIZE];
                int[] gammaB = new int[MAX_SIZE];

                // setting values for every gamma channels
                for(int i = 0; i < MAX_SIZE; ++i) {
                    gammaR[i] = (int)Math.min(MAX_VALUE_INT,
                            (int)((MAX_VALUE_DBL * Math.pow(i / MAX_VALUE_DBL, REVERSE / red)) + 0.5));
                    gammaG[i] = (int)Math.min(MAX_VALUE_INT,
                            (int)((MAX_VALUE_DBL * Math.pow(i / MAX_VALUE_DBL, REVERSE / green)) + 0.5));
                    gammaB[i] = (int)Math.min(MAX_VALUE_INT,
                            (int)((MAX_VALUE_DBL * Math.pow(i / MAX_VALUE_DBL, REVERSE / blue)) + 0.5));
                }

                // apply gamma table
                for(int x = 0; x < width; ++x) {
                    for(int y = 0; y < height; ++y) {
                        // get pixel color
                        pixel = src.getPixel(x, y);
                        A = Color.alpha(pixel);
                        // look up gamma
                        R = gammaR[Color.red(pixel)];
                        G = gammaG[Color.green(pixel)];
                        B = gammaB[Color.blue(pixel)];
                        // set new color to output bitmap
                        bmOut.setPixel(x, y, Color.argb(A, R, G, B));
                    }
                }

            result=bmOut;

            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog=ProgressDialog.show(getActivity(),"WORKING","Please wait for a moment");
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            progressDialog.dismiss();
            Toast.makeText(getActivity(),"DONE\n(TIP: Tap the original image in the home to undo)",Toast.LENGTH_LONG).show();
            filterimg.setImageBitmap(result);
            onGetImageListner.setFinishImage(filterimg);
        }
    }

    public interface OnGetImageListner{
        void setFinishImage(ImageView img);
    }

    @SuppressWarnings("deprecation")
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            onGetImageListner = (OnGetImageListner) activity;
        }catch (Exception e){

        }
    }
}
